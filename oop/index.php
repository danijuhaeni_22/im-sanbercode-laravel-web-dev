<?php

require_once("animal.php");
require_once("ape.php");
require_once("frog.php");

$sheep = new Animal("shaun");

echo "Name : " . $sheep->name . "<br>"; // "shaun"
echo "Legs : " . $sheep->legs . "<br>"; // 4
echo "Cold Blooded : " . $sheep->cold_blooded; // "no"

echo "<br>===============<br>";

$frog = new Frog("Buduk");

echo "Name : " . $frog->name . "<br>"; // "shaun"
echo "Legs : " . $frog->legs . "<br>"; // 4
echo "Cold Blooded : " . $frog->cold_blooded . "<br>"; // "no"
echo "Jump : " . $frog->Yell();

echo "<br>===============<br>";

$ape = new Ape("Kera Sakti");

echo "Name : " . $ape->name . "<br>"; // "shaun"
echo "Legs : " . $ape->legs . "<br>"; // 4
echo "Cold Blooded : " . $ape->cold_blooded . "<br>"; // "no"
echo "Jump : " . $ape->Jump();
