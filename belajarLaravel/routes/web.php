<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\CastController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [HomeController::class, 'home']);
Route::get('/register', [AuthController::class, 'register']);
Route::post('/welcome', [AuthController::class, 'kirim']);


Route::get('/table', function () {
    return view('page.table');
});
Route::get('/data-tables', function () {
    return view('page.data-table');
});


// CRUD untuk cast

// Create data
// route untuk tambah data
Route::get('/cast/create', [CastController::class, 'create']);
// route untuk menyimpan data
Route::post('/cast', [CastController::class, 'store']);

// Read data
// route untuk menampilkan semua data cast
Route::get('/cast', [CastController::class, 'index']);
// route detail data cast
Route::get('/cast/{id}', [CastController::class, 'show']);

// Update data
// route untuk mengarah ke form update dengan membawa data id
Route::get('/cast/{id}/edit', [CastController::class, 'edit']);
// route untuk update data berdasarkan id cast
Route::put('/cast/{id}', [CastController::class, 'update']);

// Delete data
// route untuk hapus data berdasarkan id cast
Route::delete('/cast/{id}', [CastController::class, 'destroy']);
