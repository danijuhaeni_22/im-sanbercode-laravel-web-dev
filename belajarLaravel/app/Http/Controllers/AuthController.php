<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function register()
    {
        return view('page.register');
    }

    public function kirim(Request $request)
    {
        $namaDepan = $request->input('first_name');
        $namaBelakang = $request->input('last_name');

        return view('page.welcome', ['namaDepan' => $namaDepan, 'namaBelakang' => $namaBelakang]);
        // return view('page.welcome', compact(["namaDepan", "namaBelakang"])); // cara lain
    }
}
