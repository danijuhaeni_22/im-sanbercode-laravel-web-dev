@extends('layouts.master')

@section('title')

Halaman Pendaftaran

@endsection


@section('content')

<h1>Buat Account Baru!</h1>

    <h2>Sign Up Form</h2>

    <form action="/welcome" method="POST">
        @csrf
        <label>First Name :</label><br><br>
        <input type="text" name="first_name"><br><br>

        <label>Last Name :</label><br><br>
        <input type="text" name="last_name"><br><br>

        <label>Gender :</label><br>
        <input type="radio" name="gender">Male<br>
        <input type="radio" name="gender">Female<br>
        <input type="radio" name="gender">Other<br><br>

        <label>Nationality :</label><br><br>
        <select name="nationality">
            <option value="indonesia">Indonesia</option>
            <option value="malaysia">Malaysia</option>
            <option value="singapura">Singapura</option>
            <option value="australia">Australia</option>
        </select><br><br>

        <label>Language Spoken :</label><br><br>
        <input type="checkbox" name="bahasa_indonesia">Bahasa Indonesia <br>
        <input type="checkbox" name="english">English <br>
        <input type="checkbox" name="other">Other <br><br>

        <label>Bio :</label><br><br>
        <textarea name="bio" rows="10" cols="30"></textarea><br>

        <input type="submit" value="Sign Up">
    </form>

@endsection
